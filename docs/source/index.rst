.. UNAGI documentation master file, created by
   sphinx-quickstart on Sun Jan 19 16:42:12 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to UNAGI's documentation!
=================================

UNAGI (U-Net Adaptive Generalized Image Binarization) is a python package to perform
binarization on document images to improve OCR results. UNAGI performs
segmantic segmentation on the image and creates a binary image.
U-net is used as the model architecture to train the deep learning model.
UNAGI comes with a pretrained model trained on `DIBCO <http://vc.ee.duth.gr/dibco2019/>`_ dataset.


Tutorials
---------
.. toctree::
   :maxdepth: 2

   quick_start_guide
   modules

In-depth
--------
.. toctree::
   :maxdepth: 1

   ann
   back_prop
   cnn
   unet
   ocr

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
