Sub Packages
============

unagi.utils
-----------
Unagi has one sub package called ``utils`` which contains several methods and classes
that are used to create the dataset and train the model.

unagi.utils.augmentor\_utils
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Different augmentation objects are created to use during the training. This module
contains those different augmentation objects used in the training.

.. automodule:: unagi.utils.augmentor_utils
   :members:
   :undoc-members:
   :show-inheritance:

unagi.utils.callback\_utils
^^^^^^^^^^^^^^^^^^^^^^^^^^^
Callbacks for visulations of the training and saving the model are created using
the objects in callback_utils module.

.. automodule:: unagi.utils.callback_utils
   :members:
   :undoc-members:
   :show-inheritance:

unagi.utils.img\_processing\_utils
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Different image manipulation techniques are used in UNAGI. 
Dataset module uses different methods to create the images parts and
preprocess the images during training.

.. automodule:: unagi.utils.img_processing_utils
   :members:
   :undoc-members:
   :show-inheritance:

unagi.utils.metric\_utils
^^^^^^^^^^^^^^^^^^^^^^^^^
UNAGI uses different loss functions and metrics to measure the performace of the model
training. This module contains different loss functions and metrics used in the training
and validation of the model.

.. automodule:: unagi.utils.metric_utils
   :members:
   :undoc-members:
   :show-inheritance:

unagi.utils.model\_utils
^^^^^^^^^^^^^^^^^^^^^^^^
Contains methods used to create the U-net architechture used in the model training
such as DoubelConv layer, upsampling, and downsampling paths.

.. automodule:: unagi.utils.model_utils
   :members:
   :undoc-members:
   :show-inheritance:


.. automodule:: unagi.utils
   :members:
   :undoc-members:
   :show-inheritance:
