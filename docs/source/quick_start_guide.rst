Quick Start Guide
=================

Installation
------------

``UNAGI`` requires Python 3.5+.

The current development version is available on `Gitlab
<https://gitlab.com/vkolagotla/UNAGI>`__. Use :command:`git` and
:command:`python setup.py` to install it:

Getting ``UNAGI`` is super easy, just clone it from gitlab and install the latest stable version by doing:

.. code-block:: bash

   git clone https://gitlab.com/vkolagotla/UNAGI
   cd UNAGI
   python setup.py install

Features
--------

- Create training dataset.
- Train a U-net model.
- Binarize an image with the saved model.
- Python package to use in your existing code base.
- CLI tool to test in command line.
- Dockerfile to run in a container.
- Gradio app to test the model in a web browser via UI.

Example Usage
-------------
Following code block shows how to use some of unagi package modules

.. code-block:: python

    # import unagi package
    import unagi
    
    # create the input train data for the model
    # dataset method takes "input_dir" as input and saves the images and
    # their respective binary masks into "output_dir"
    unagi.dataset(input="input_dir", output="output_dir")

    # uses the images in "train_img_dir" to train the network and saves the weights 
    # in "weights" folder in the current directory
    # "vis_img_dir" folder is used to observe the training process with some sample images
    # where the model is tested for each best saved model weights
    unagi.train(input="train_img_dir", vis="vis_img_dir")

    # converts all the images in "input_img_dir" to binary images with the model
    # available in weights folder and saves the binary images into "output_dir_to_save_imgs"
    unagi.binarize(input="input_img_dir", output="output_dir_to_save_imgs")

**How to use the CLI tool:**

Once you install the package, you can use the CLI interface to run the modules like.

.. code-block:: bash

    unagi dataset --input="input_dir/" --output="output_dir/"
    unagi train --input="input_dir/" --vis="vis_dir/"
    unagi binarize --input="input_dir/" --output="output_dir/"

Run `unagi --help` to see the list of available commands and their args.

.. code-block:: bash

    unagi --help
    unagi dataset --help
    unagi train --help
    unagi binarize --help

You can also run the python files directly to use any of the 3 modules like below.

.. code-block:: bash

    python3 unagi/dataset.py --input="input_dir/" --output="output_dir/"

**How to use Gradio app UI:**

Simple run the ``app.py`` file using ``python`` or ``Gradio`` in CLI and it will create a web app by default
at ``http://localhost:7860/`` where you can test the model with your own images by uploading them.


Tips
----
.. tip:: Use Focal loss as a loss function if you are planning on training the model.

.. tip:: Handwritten text can be suppressed from typewritten documents with proper training data.

.. tip:: Make sure that the input image and ground truth image are aligned on character level without any shifts.

Notes
-----
.. note:: Pretrained model weights are taken from `Robin <https://github.com/masyagin1998/robin>`_.

Warnings
--------
.. warning:: The pretrained model is trained on both handwritten and printed text. This could result in washing out of text after binarization for some type of documents such as old typewritten documents.
