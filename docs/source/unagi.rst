Modules
=======
Unagi has three submodules binarize, dataset and train.
Each submodule has a main method, which can be used to create a dataset,
train the model with the created data and then binarize the image using saved model weights.

unagi.binarize
--------------
Binarize module uses a pretrained model saved from the model training and performs
the model prediction i.e binarization on the given image.

.. automodule:: unagi.binarize
   :members:
   :undoc-members:
   :show-inheritance:

unagi.dataset
-------------
Dataset module can be used to create the traindata set. It takes a folder with input image and it's corrosponding ground truth image.
Image name should end with **_in** and ground truth image name should end with **_gt**. Input and ground truth images should have same file extention.

.. tip::

   Consider saving the images in **PNG** format and not **JPG**. Saving binary images in JPG format
   would make the image to carry some gray level pixels.

Images are cropped into smaller image parts based on the input size of the U-net model.
Output folder will contain two sub folders such as **in** and **gt**. 
**in** folder contains the input images and **gt** contains the respective ground images.

.. automodule:: unagi.dataset
   :members:
   :undoc-members:
   :show-inheritance:

unagi.train
-----------
Train module is used to train the U-net model. Train dataset is split into
train, validation and test datasets to use in model training.
Best fitting weights are saved for each epoch
and the model performance can be visualized by using some images to test the model performance.
These images are independent of the training set.

Loss functions can be selected from the available options and the train data is augmented
on fly during the training to make the model robust to the distortions in data.

.. automodule:: unagi.train
   :members:
   :undoc-members:
   :show-inheritance:

unagi.cli
---------
Command line interface for the unagi package. It can be used to create dataset, train the model and binarize the image.

.. code-block:: bash

   $ unagi --help
   usage: unagi [-h] [-v] {dataset,train,binarize} ...

   command-line interface for Unagi package

   optional arguments:
   -h, --help            show this help message and exit
   -v, --version         show package version and exit

   available commands:
   {dataset,train,binarize}
      dataset             Create dataset to train unagi model
      train               Train the unagi model
      binarize            Use the model weights to binarize images

.. automodule:: unagi.cli
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: unagi
   :members:
   :undoc-members:
   :show-inheritance:
