# UNAGI

<img src="static/logo/unagi.png" height="120" width="120">

**U**-**N**et **A**daptive **G**eneralized **I**mage Binarization for Documents

## How to use UNAGI

**UNAGI** is a python package which uses a deep leaning model to perform binarization on document images. The package lets you retrain train the model again with very little effort. The package modules also allows args to be passed to them via command line. The package also has a CLI interface to run the modules.

Read the docs at <a href="https://unagi.readthedocs.io/en/latest/">UNAGI-Documentation</a>

### Installation

```bash
    git clone https://gitlab.com/vkolagotla/UNAGI
    cd UNAGI
    python3 setup.py install
```
or you can use the `Dockerfile` in the repo to build a docker image and run the package in a container.

### CLI Usage
Once you install the package, you can use the CLI interface to run the modules like.

```bash
    unagi dataset --input="input_dir/" --output="output_dir/"
    unagi train --input="input_dir/" --vis="vis_dir/"
    unagi binarize --input="input_dir/" --output="output_dir/"
```
Run `unagi --help` to see the list of available commands and their args.

```bash
    unagi --help
    unagi dataset --help
    unagi train --help
    unagi binarize --help
```

You can also run the python files directly to use any of the 3 modules like below.

```bash
    python3 unagi/dataset.py --input="input_dir/" --output="output_dir/"
```

### Package Usage

Train the model with:

`unagi.train.main(input="input_dir/", vis="vis_dir/")`

Prepare train data with:

`unagi.dataset.main(input="input_dir/", output="output_dir/")`

Binarize an image with trained model:

`unagi.binarize.main(input="input_dir", output="output_dir")`

### Package Testing

#### Linting, formatting checking
Linting and formatting of the code with: [pylama](https://github.com/klen/pylama)

`pip install pylama==8.4.1 pycodestyle==2.11.1`

`pylama unagi/`

#### Type checking
Type check the code with: [mypy](https://mypy.readthedocs.io/en/stable/)

`pip install mypy==1.5.1`

`mypy unagi/`

#### Unit and integration testing
Test the code with: [pytest](https://docs.pytest.org/en/6.2.x/index.html)

`pip install pytest==5.3.2 pytest-cov==2.8.1`

`pytest --cov-report term-missing --disable-pytest-warnings --cov=unagi/ tests/`

Note: These are not included in the package requirements.txt file to speed up the CI pipeline time. You to install them manually, since they are common common dev tools to writing python code/packages.

#### Testing binarization results locally with Gradio UI

You can also test the binarization results locally with [Gradio](https://www.gradio.app/).
Gradio provides a simple UI to test ML/DL models locally and also deploy them to a server.

You can install gradio with `pip install gradio==3.50.2` and run the `app.py` file in terminal as `python3 app.py` or `gradio app.py` and it will server a web app on `http://127.0.0.1:7860/` by default. You can open the link in your browser and upload an image to test the binarization results. Only one image is allowed to be uploaded at a time.

### Docs Testing
You can generate the docs locally and test them with [sphinx](https://www.sphinx-doc.org/en/master/):

`pip install docs/requirments.txt`

`cd docs`

`sphinx-build -b html source/ build/`

Docs for the package are generated in the `docs/build` directory. You can open the `index.html` file in your browser to view the docs.

Source files for the module docs are in `docs/source` directory with `rst` file format. You can edit them and generate the docs again to see the changes if you add more modules, methods to the package. Do not create the `source/` dir in the `docs/` dir if its already there. Sphinx will replace the current `source/` dir with a new and basic `source/` dir.

You can edit `modules.rst`, `unagi.rst`, `unagi.utils.rst` files to edit the package docs.

Docs for the package are also hosted on [readthedocs](https://unagi.readthedocs.io/en/latest/) and here on [gitlab pages](https://vkolagotla.gitlab.io/UNAGI/).

If you want to add docs as one more stage to CI pipeline and host them via gitlab pages, all you need to do create the `build/` dir in the CI or locally and copy it into `/public/` dir like you do for any other static website to host them in pages.

## Future Work

1. Add more tests.
2. Update the weight file by training the model with new data. (Need access to a GPU)

## References

### Dataset

- [DIBCO](https://yadi.sk/d/_91feeU21y3riA) - 2009 - 2018 competition datasets
- [Borders](https://yadi.sk/d/p6R8kgPP98BZtw) - Small dataset containing bad text boundaries.
- [Palm Leaf Manuscript](https://yadi.sk/d/sMJxS3IGyTRJEA) - Palm Leaf Manuscript dataset from ICHFR2016 competition;

### Articles

- [U-net](https://arxiv.org/abs/1505.04597) - article about U-net convolutional network architecture.

## Thanks to

[Mikhail Masyagin](https://github.com/masyagin1998) - for making [robin](https://github.com/masyagin1998/robin) available to everyone.

## Package Doc Tree

Directory structure of the package and test dirs

```shell
unagi/
├── binarize.py
├── cli.py
├── dataset.py
├── __init__.py
├── train.py
└── utils
    ├── augmentor_utils.py
    ├── callback_utils.py
    ├── img_processing_utils.py
    ├── __init__.py
    ├── metric_utils.py
    ├── model_utils.py
tests/
├── binarize_test.py
├── cli_test.py
├── conftest.py
├── data
├── dataset_test.py
├── __init__.py
├── train_test.py
└── utils
    ├── augmentor_utils_test.py
    ├── img_processing_utils_test.py
    ├── metric_utils_test.py
    ├── model_utils_test.py
```