#!/usr/bin/python3

import setuptools

with open("README.md", "r") as f:
    unagi_description = f.read()

with open("requirements.txt") as f:
    required = f.read().splitlines()

setuptools.setup(
    name="unagi",
    version="0.5.3",
    author="Venkata Kolagotla",
    author_email="vkolagotla@pm.me",
    description="A package to perform U-net binarization on doc images",
    long_description=unagi_description,
    url="https://gitlab.com/vkolagotla/UNAGI.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=required,
    entry_points={
        "console_scripts": [
            "unagi=unagi.cli:main",
        ],
    },
)
