from __future__ import print_function

import os
import shutil

from unagi import dataset


def test_split_img_overlay(in_img_array):
    """Split image into tails of small images"""
    img_processor = dataset.ImageProcessor()
    output = img_processor.split_img_overlay(in_img_array)

    assert isinstance(output[0], list)
    assert isinstance(output[1], int)
    assert isinstance(output[2], int)


def test_main(dataset_data, data_path):
    """Prepare image tails for model training"""
    dataset.main(dataset_data, output_path=data_path + "/output_dataset")

    assert os.path.isdir(data_path + "/output_dataset")
    shutil.rmtree(data_path + "/output_dataset")
