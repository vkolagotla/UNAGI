from __future__ import print_function

import os
import shutil

from unagi import train


def test_main(train_data, vis_data, base_path):
    """Train model with a sample test dataset"""
    train.main(
        train_data,
        vis_data,
        loss="dice",
        debug=base_path + "/junk",
        epochs=1,
        batchsize=2,
        weights_path=base_path + "/junk/weights.hdf5",
    )

    assert os.path.isdir(base_path + "/junk")
    shutil.rmtree(base_path + "/junk")
    os.remove(vis_data + "/02_in.gif")
    shutil.rmtree(vis_data + "/02_in_frames")
