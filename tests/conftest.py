import os

import cv2
import numpy as np
import pytest
import tensorflow as tf


@pytest.fixture(scope="module")
def base_path():
    """Tests dir path"""
    return os.path.dirname(os.path.abspath(__file__))


@pytest.fixture(scope="module")
def data_path(base_path):
    """Test data dir path"""
    base_data_path = base_path + "/data"
    return base_data_path


@pytest.fixture(scope="module")
def bin_data_path(data_path):
    """Test data dir path for sample color/input image"""
    data_bin_path = data_path + "/bin_img"
    return data_bin_path


@pytest.fixture(scope="module")
def in_img_path(bin_data_path):
    """Sample input image path from train data"""
    img_in_path = bin_data_path + "/01_in.png"
    return img_in_path


@pytest.fixture(scope="module")
def gt_img_path(data_path):
    """Sample ground truth image path from train data"""
    img_gt_path = data_path + "/01_gt.png"
    return img_gt_path


@pytest.fixture(scope="module")
def bin_img_path(data_path):
    """Sample binary input image path"""
    img_bin_path = data_path + "/01_bin_in.png"
    return img_bin_path


@pytest.fixture(scope="module")
def in_img_array(in_img_path):
    """Sample input image array from train data"""
    img_in_array = cv2.imread(in_img_path)
    return img_in_array


@pytest.fixture(scope="module")
def gt_img_array(gt_img_path):
    """Sample ground truth image array from train data"""
    img_gt_array = cv2.imread(gt_img_path)
    return img_gt_array


@pytest.fixture(scope="module")
def y_true(gt_img_path):
    """Sample input tensor from input image"""
    img_gt = cv2.imread(gt_img_path)
    img_gt = np.asarray(img_gt, np.float32)
    y_true = tf.convert_to_tensor(img_gt, np.float32)
    return y_true


@pytest.fixture(scope="module")
def y_pred(bin_img_path):
    """Sample input tensor from ground truth(binary) image"""
    img_bin = cv2.imread(bin_img_path)
    img_bin = np.asarray(img_bin, np.float32)
    y_pred = tf.convert_to_tensor(img_bin, np.float32)
    return y_pred


@pytest.fixture(scope="module")
def train_data(data_path):
    """Train data prepared from original dataset"""
    train_data_path = data_path + "/train"
    return train_data_path


@pytest.fixture(scope="module")
def dataset_data(data_path):
    """Original dataset"""
    dataset_data_path = data_path + "/dataset"
    return dataset_data_path


@pytest.fixture(scope="module")
def vis_data(data_path):
    """Dir path with images to visualize during training"""
    vis_data_path = data_path + "/vis"
    return vis_data_path


@pytest.fixture(scope="module")
def weights_path():
    """Weights path fetched from weights dir"""
    package_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    weights_file = os.path.join(package_dir, "weights", "bin_weights_file.hdf5")
    print(f"weights_file: {weights_file}")
    return weights_file
