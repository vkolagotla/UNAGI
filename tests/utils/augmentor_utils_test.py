import cv2
import numpy as np

from unagi.utils.augmentor_utils import InvertPartAugmentor


def test_invert(bin_img_path):
    """Test the __invert__ method of InvertPartAugmentor"""
    probability = 0.5  # probability of operation being performed
    augmentor = InvertPartAugmentor(probability)

    img = img = cv2.imread(bin_img_path, cv2.IMREAD_GRAYSCALE)
    inverted_img = augmentor.__invert__(img)
    inverted_img_np = np.array(inverted_img)

    assert inverted_img_np is not None
    assert inverted_img_np.shape == img.shape
    assert inverted_img_np.dtype == img.dtype
