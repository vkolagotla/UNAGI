from __future__ import print_function

import tensorflow as tf
from tensorflow.keras.layers import Input

from unagi.utils import model_utils


def test_double_conv_layer():
    """Test double convolution layer."""
    input_size = Input((448, 448, 1))
    conv_layer = model_utils.ModelLayers().double_conv_layer(input_size, 32)
    assert isinstance(conv_layer, tf.Tensor)


def test_down_layer():
    """Test down layer."""
    inputs = Input((448, 448, 1))
    down_layer, pool_layer = model_utils.ModelLayers().down_layer(inputs, 32)
    assert isinstance(down_layer, tf.Tensor)
    assert isinstance(pool_layer, tf.Tensor)


def test_up_layer():
    """Test up layer."""
    inputs = Input((448, 448, 1))
    down_layer, pool_layer = model_utils.ModelLayers().down_layer(inputs, 32)
    up_layer = model_utils.ModelLayers().up_layer(pool_layer, down_layer, 32)
    assert isinstance(up_layer, tf.Tensor)


def test_unet():
    """Test unet model."""
    unet_model = model_utils.UNAGIModel().unet()
    assert isinstance(unet_model, tf.keras.Model)
