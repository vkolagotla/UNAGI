import tensorflow as tf
from unagi.utils.metric_utils import ModelMetrics


def test_dice_coef(y_true, y_pred):
    """Dice coefficient test"""
    dice_coef = ModelMetrics.dice_coef(y_true, y_pred)
    assert dice_coef.dtype == float


def test_dice_coef_loss(y_true, y_pred):
    """Dice loss test"""
    dice_loss = ModelMetrics.dice_coef_loss(y_true, y_pred)
    assert dice_loss.dtype == float


def test_jaccard_coef(y_true, y_pred):
    """Jaccard coefficient test"""
    jaccard_coef = ModelMetrics.jacard_coef(y_true, y_pred)
    assert jaccard_coef.dtype == float


def test_jaccard_coef_loss(y_true, y_pred):
    """Jaccard loss test"""
    jaccard_loss = ModelMetrics.jacard_coef_loss(y_true, y_pred)
    assert jaccard_loss.dtype == float


def test_weighted_cross_entropy(y_true, y_pred):
    """Weighted cross entropy loss test"""
    loss_func = ModelMetrics.weighted_cross_entropy(pos_weight=0.2)
    assert callable(loss_func)

    loss = loss_func(y_true, y_pred)
    assert tf.is_tensor(loss) and loss.dtype == tf.float32


def test_focal_loss(y_true, y_pred):
    """Focal loss test"""
    loss_func = ModelMetrics.focal_loss(alpha=0.25, gamma=2.0)
    assert callable(loss_func)

    loss = loss_func(y_true, y_pred)
    assert tf.is_tensor(loss) and loss.dtype == tf.float32


def test_travesky(y_true, y_pred):
    """Travesky test"""
    travesky = ModelMetrics.tversky(y_true, y_pred)
    assert travesky.dtype == float


def test_tversky_loss(y_true, y_pred):
    """Tversky loss test"""
    tversky_loss = ModelMetrics.tversky_loss(y_true, y_pred)
    assert tversky_loss.dtype == float


def test_focal_tversky_loss(y_true, y_pred):
    """Focal tversky loss test"""
    focal_tversky_loss = ModelMetrics.focal_tversky(y_true, y_pred)
    assert focal_tversky_loss.dtype == float
