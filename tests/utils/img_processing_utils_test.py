from __future__ import print_function

import numpy as np
import cv2

from unagi.utils.img_processing_utils import ImageUtils


def test_normalize_in(in_img_array):
    """Normalize input image values to 0-1"""
    out_img = ImageUtils.normalize_in(in_img_array)
    assert isinstance(out_img, np.ndarray)


def test_normalize_gt(gt_img_array):
    """Normalize ground truth image values to 0-1"""
    out_img = ImageUtils.normalize_gt(gt_img_array)
    assert isinstance(out_img, np.ndarray)


def test_add_border(in_img_array, data_path):
    """Add a boarder to the image"""
    out_tuple = ImageUtils.add_border(in_img_array, 128, 128)
    assert isinstance(out_tuple[0], np.ndarray)
    assert isinstance(out_tuple[1], int)
    assert isinstance(out_tuple[2], int)


def test_split_img(in_img_array):
    """splits the image into small image tails"""
    out_list = ImageUtils.split_img(in_img_array, 128, 128)
    assert len(out_list) > 0
    assert isinstance(out_list, list)
    assert isinstance(out_list[0], np.ndarray)
    assert isinstance(out_list[-1], np.ndarray)


def test_combine_imgs(in_img_array, data_path):
    """Combine image tails to the original image size"""
    split_imgs = ImageUtils.split_img(in_img_array, 128, 128)
    combined_img = ImageUtils.combine_imgs(split_imgs[0], 128, 128)
    assert isinstance(combined_img, np.ndarray)


def test_postprocess_img(gt_img_array):
    """Postprocess the binary image"""
    gray_img = cv2.cvtColor(gt_img_array, cv2.COLOR_BGR2GRAY)
    out_bin_img = ImageUtils.postprocess_img(gray_img)
    assert isinstance(out_bin_img, np.ndarray)
