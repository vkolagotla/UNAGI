from __future__ import print_function

import shutil

from unagi import binarize


def test_main(data_path, bin_data_path):
    """Saved U-net model output type for a dir of images"""
    output = binarize.main(bin_data_path, output_path=data_path + "/output")
    assert isinstance(output, list)
    # Remove the directory created
    shutil.rmtree(data_path + "/output")
