import os
import shutil
import subprocess

import pytest

from unagi.cli import get_version


def test_get_version():
    """Test the get_version function"""
    assert isinstance(get_version(), str)


def test_pkg_version():
    """Test the package version"""
    result = subprocess.run(["unagi", "--version"], capture_output=True, text=True)
    assert result.returncode == 0
    # check if the output contains "unagi" in it
    assert "unagi" in result.stdout


def test_dataset_command():
    """Test the dataset command"""
    result = subprocess.run(
        ["unagi", "dataset", "--input_path", "./input", "--output_path", "./output"],
        capture_output=True,
        text=True,
    )
    assert result.returncode == 0


@pytest.mark.skip(
    reason="Skipping this test as train.py will be tested later(reduces test time)"
)
def test_train_command(train_data, vis_data, base_path):
    """Test the train command"""
    result = subprocess.run(
        [
            "unagi",
            "train",
            "--input_path",
            train_data,
            "--vis",
            vis_data,
            "--loss",
            "dice",
            "--debug",
            base_path + "/data/junk",
            "--epochs",
            "1",
            "--batchsize",
            "2",
            "--weights_path",
            base_path + "/junk/weights.hdf5",
        ],
        capture_output=True,
        text=True,
    )
    assert result.returncode == 0
    shutil.rmtree(base_path + "/junk")
    os.remove(vis_data + "/02_in.gif")
    shutil.rmtree(vis_data + "/02_in_frames")


def test_binarize_command(bin_data_path, weights_path):
    """Test the binarize command"""
    result = subprocess.run(
        [
            "unagi",
            "binarize",
            "--input_path",
            bin_data_path,
            "--output_path",
            "output",
            "--weights_path",
            weights_path,
        ],
        capture_output=True,
        text=True,
    )
    assert result.returncode == 0
    shutil.rmtree("output")
