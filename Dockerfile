# Use the python:3.8 base image
FROM python:3.8

# Set the working directory to /app
WORKDIR /app

# Copy the entire project into the Docker image
COPY . /app

# Install the necessary dependencies for each stage
RUN pip install --upgrade pip
# formatting check dependencies
RUN pip install pylama==8.4.1 pycodestyle==2.11.1 black=23.10.1
# type check dependencies
RUN pip install mypy==1.5.1
# test dependencies
RUN pip install pytest==5.3.2 pytest-cov==2.8.1
# docs dependencies
RUN pip install -r docs/requirements.txt

# install the UNAGI package
RUN pip install .

# Cleanup unnecessary files
RUN rm -rf .git

# Specify a non-root user
RUN useradd -m unagi_user
USER unagi_user

# Set the entry point to run the tests
ENTRYPOINT ["pytest", "--cov-report", "term-missing", "--cov=unagi"]

# Run formatting, type checking tests using pylama, mypy, and check unagi version
CMD ["pylama unagi && mypy unagi && unagi --version"]
