import argparse
import pkg_resources  # type: ignore

# Import package modules
from unagi.dataset import main as dataset_main
from unagi.train import main as train_main
from unagi.binarize import main as binarize_main


def get_version() -> str:
    """Get the version of the package to print in CLI.

    Returns
    -------
    str
        version of the package
    """

    try:
        version = pkg_resources.get_distribution("unagi").version
        return version
    except Exception:
        return "unknown"


def main() -> None:
    """Main function for the CLI entry point.

    Returns
    -------
    None
    """

    parser = argparse.ArgumentParser(
        description="command-line interface for Unagi package"
    )
    parser.add_argument(
        "-v",
        "--version",
        action="version",
        version=f"%(prog)s {get_version()}",
        help="show package version and exit",
    )

    subparsers = parser.add_subparsers(title="available commands", dest="command")

    # Subparser for dataset command
    dataset_parser = subparsers.add_parser(
        "dataset", help="Create dataset to train unagi model"
    )

    dataset_parser.add_argument(
        "--input_path", type=str, default="./input", help="input path"
    )
    dataset_parser.add_argument(
        "--output_path", type=str, default="./output", help="output path"
    )
    dataset_parser.add_argument(
        "--shuffle", type=bool, default=True, help="shuffle data"
    )
    dataset_parser.add_argument("--size_x", type=int, default=128, help="size x")
    dataset_parser.add_argument("--size_y", type=int, default=128, help="size y")
    dataset_parser.add_argument("--step_x", type=int, default=128, help="step x")
    dataset_parser.add_argument("--step_y", type=int, default=128, help="step y")
    dataset_parser.add_argument(
        "--processes", type=int, default=4, help="number of processes"
    )

    # Subparser for train command
    train_parser = subparsers.add_parser("train", help="Train the unagi model")

    train_parser.add_argument(
        "--input_path", type=str, default="./input", help="train dataset path"
    )
    train_parser.add_argument(
        "--vis", type=str, default="./vis", help="visualization data path"
    )
    train_parser.add_argument(
        "--debug", type=str, default="./train_logs", help="debug data path"
    )
    train_parser.add_argument(
        "--loss",
        type=str,
        default="dice_coef_loss",
        help="loss function",
    )
    train_parser.add_argument("--epochs", type=int, default=1, help="number of epochs")
    train_parser.add_argument("--batchsize", type=int, default=32, help="batch size")
    train_parser.add_argument(
        "--augmentate", type=bool, default=True, help="augmentate data"
    )
    train_parser.add_argument("--train_split", type=int, default=80, help="train split")
    train_parser.add_argument(
        "--val_split", type=int, default=10, help="validation split"
    )
    train_parser.add_argument("--test_split", type=int, default=10, help="test split")
    train_parser.add_argument(
        "--weights_path", type=str, default="./bin_weights.hdf5", help="weights path"
    )
    train_parser.add_argument("--num_gpus", type=int, default=1, help="number of gpus")
    train_parser.add_argument(
        "--extraprocesses", type=int, default=0, help="extra processes"
    )
    train_parser.add_argument("--queuesize", type=int, default=10, help="queue size")

    # Subparser for binarize command
    binarize_parser = subparsers.add_parser(
        "binarize", help="Use the model weights to binarize images"
    )
    binarize_parser.add_argument(
        "--input_path", type=str, default=".input/", help="Input path"
    )
    binarize_parser.add_argument(
        "--output_path", type=str, default=".output/", help="Output path"
    )
    binarize_parser.add_argument(
        "--weights_path", type=str, default=None, help="Weights path"
    )
    binarize_parser.add_argument("--batchsize", type=int, default=2, help="Batch size")

    args = parser.parse_args()

    # Call the appropriate function based on the subcommand
    if args.command == "dataset":
        # Call the dataset function
        dataset_main(
            args.input_path,
            args.output_path,
            args.shuffle,
            args.size_x,
            args.size_y,
            args.step_x,
            args.step_y,
        )
    elif args.command == "train":
        # Call the train function
        train_main(
            args.input_path,
            args.vis,
            args.debug,
            args.loss,
            args.epochs,
            args.batchsize,
            args.augmentate,
            args.train_split,
            args.val_split,
            args.test_split,
            args.weights_path,
            args.num_gpus,
            args.extraprocesses,
            args.queuesize,
        )
    elif args.command == "binarize":
        # Call the binarize function
        binarize_main(
            args.input_path, args.output_path, args.weights_path, args.batchsize
        )
    else:
        args.print_help()


if __name__ == "__main__":
    main()
