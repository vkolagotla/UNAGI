import cv2
import numpy as np
import gradio as gr
from tensorflow.keras.optimizers import Adam

from unagi.utils.model_utils import UNAGIModel
from unagi.utils.metric_utils import ModelMetrics
from unagi.utils.img_processing_utils import ImageUtils


def unagi_binarize(input_path: str) -> np.ndarray:
    """Binarize images from upload and return binary image as np.array for Garadio UI.

    Parameters
    ----------
    input_path: str
        path to image

    Returns
    -------
    numpy.ndarray
        binary image array
    """

    # check if the input path is an image or not, like png, jpg, jpeg, webp
    if not input_path.endswith((".png", ".jpg", ".jpeg", ".webp")):
        raise ValueError("Input path is not an image")

    model = UNAGIModel().unet()
    model.compile(
        optimizer=Adam(lr=1e-4),
        loss=ModelMetrics.dice_coef_loss,
        metrics=[ModelMetrics.dice_coef],
    )
    model.load_weights("weights/bin_weights_file.hdf5")

    img = cv2.imread(input_path, cv2.IMREAD_GRAYSCALE).astype(np.float32)
    bin_img = ImageUtils.binarize_img(img, model, batchsize=2)

    return bin_img


demo = gr.Interface(unagi_binarize, gr.Image(type="filepath"), "image")
demo.launch()
